import { products } from "./index";
const titleDisplay = document.querySelector("#title");
const tabellaDisplay = document.querySelector("#tabellaCarta");
const totalProductDisplay = document.querySelector("#totalProduct");
const totalPriceDisplay = document.querySelector("#totalPrice");
//Welcome
export function populateUser(isReturning, nomePrimo) {
    const accesso = isReturning ? "Bentornato" : "";
    titleDisplay.innerHTML = `<h2>${accesso} ${nomePrimo}, il tuo carrello contiene: </h2>`;
}
//Totale cart value
export function totalValue(prodotti, prezzo) {
    let somma = 0;
    for (let i = 0; i < products.length; i++) {
        somma = products[i].prezzo + somma;
    }
    const nProdotti = products.length > 0 ? `Hai ${products.length}` : "Non hai ";
    totalProductDisplay.innerHTML = nProdotti;
    const pProdotti = products.length > 0 ? `Il totale è di ${somma} €` : "";
    totalPriceDisplay.innerHTML = pProdotti;
}
// product item
export const creaProdotto = (id, prodotti, immagine, titoloProdotto, costoDiConsegna, prezzo, spedizioneData) => {
    for (let i = 0; i < products.length; i++) {
        const prodottoItem = document.createElement("div");
        tabellaDisplay.appendChild(prodottoItem);
        prodottoItem.classList.add("prodottoItem");
        const image = document.createElement("img");
        image.setAttribute("src", products[i].immagine);
        prodottoItem.innerHTML = `
    <button class="removeBtn" id="${products[i].id}">❌</button>
          <div id="img">
            <img src="${products[i].immagine}" alt="" />
          </div>
          <div>
            <span>Prodotto:</span>
            <div id="descrizioneProdottoItem">
              ${products[i].titoloProdotto}
            </div>
          </div>
          <div>
            <span>Costo di consegna:</span>
            <div id="prezzoSpedizione">${products[i].costoDiConsegna}</div>
          </div>
          <div>
            <span class="spedizione">Spedizione</span>
            <div id="dataSpedizione">${products[i].spedizioneData}</div>
          </div>
          <div>
            <button id="addButton" class="btn">+</button
            ><input type="text" /><button class="btn" id="removeButton">
              -
            </button>
          </div>
          <div>
            <span>Totale</span>
            <div id="prezzoTotale">${products[i].prezzo}</div>
          </div>
    `;
    }
    const deleteButton = document.querySelector(".removeBtn");
    //Delete button
    deleteButton.addEventListener("click", (e) => {
        console.log("click");
        console.log(e.target);
    });
};
//# sourceMappingURL=cartItem.js.map